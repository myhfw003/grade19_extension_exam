# 19级软件第二学期期末考试

#### 介绍
19级软件第二学期期末考试，适用于软件专业

#### 考试内容
使用Sqlserver、Winform和Ado.Net技术，完成一个具有简单登录、注册、增删改查功能的桌面应用程序。

#### 步骤和要求
1. 利用Sqlserver，使用sql语句完成建库、建表，插入测试数据等步骤，并将以上sql语句保存为DbInit.sql文件；
    + **数据库名称：Blogs**
    + **用户表名称：Users，包含Id、用户名(Username)、密码(Password)等信息**
    + **文章表名称：Articles，包含Id、文章标题(Title)、文章作者(Author)、创建时间(CreatedTime)等信息**
2. 使用Visual Studio 2017或Visual Studio 2019，创建一个Winform类型的项目，完成登录、注册功能,完成对文章的增加、修改、删除和查询等功能；
3. 将前面两步产生的sql文件和解决方案文件，提交到各自班级在码云的仓库；